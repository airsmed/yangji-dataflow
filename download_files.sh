# !/bin/bash
DIR="$(pwd)"
DCM_DIR="/mnt/airsfs2/ChangyoungKwon/datasets/yangji-report"

# expected downloaded files
EXPECTED_NAMES=("01-device" "02-cmove-before" "03-cmove-after" "04-cstore-before" "05-cstore-after")

if [ ! -d ${DCM_DIR} ]; then
	echo "unable to fetch from fileserver"
	exit 1 
fi

for VAR in $(find $DCM_DIR -mindepth 1 -maxdepth 1 -type d); do
	echo "...attempt to download $(basename ${VAR})"
	cp -r -n ${VAR} ./
done

DOWNLOADED=$(find $DIR -mindepth 1 -maxdepth 1 -type d -not -path "**/.git" -exec basename {} \;)

# check unexpected directory
UNEXPECTED=($(echo "${EXPECTED_NAMES[@]} ${DOWNLOADED[@]}" | tr ' ' '\n' | sort | uniq -u | tr '\n' ' '))

if [ "${#UNEXPECTED[@]}" -ne 0 ]; then
	echo "unexpected directory : ${UNEXPECTED[@]}"
	exit 1
fi

echo "success"
exit 0
