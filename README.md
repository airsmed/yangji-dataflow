# 양지병원 dicom 확인 결과

테크하임 PACS와 integration하면서 아래와 같은 부분들이 예상과 달랐다.

1. c-find 시 한글 깨짐
2. PACS로부터 파일 다운로드 요청(`c-move`), 파일 저장 요청(`c-find`) 시 다이콤 태그 변경
3. PACS로부터 받은 다이콤을 worker에 recon 요청 시 에러 발생
4. PACON을 거친 다이콤이 원본 study와 동일한 study로 인식되지 않음

본 문서는 각 현상을 설명, action-item을 제시하기 위해서 작성하였다.

## 1. 목차

## 2. 현상

### 2.1. 한글 깨짐

  한글이 깨지는 현상은 `findscu` 가 적절한 encoding을 제공하지 않아서 발생하였다. 테크하임은 한글 지원을 위해 character set "ISO 2022 IR 149"을 사용한다. SCU가 `c-find` 요청을 보낼 때 character-set을 명시하던가, 추가적으로 encoding을 해야한다.

### 2.2. PACS에 의한 다이콤 태그 변경

우리가 실험하면서 MR에서 직접 넣은 다이콤 파일들은 아래와 같은 흐름을 거쳤고, 그 때마다 태그가 변경되었다.

#### 2.2.1. Device -> MR

 크게는 (1) encoding 변경, (2) meta-info 삭제, (3) 압축, (4) siemens private-tag 삭제(혹은 숨김?), (5) group-length 추가(legacy라고 함), (6) 자체 private-tag 추가가 이루어진다.

|Tags | Device | MR |
|---|---|---|
|(0002, 0000) File Meta Information Group Length           | 178                                                       | 64                                                        |
|(0002, 0001) File Meta Information Version                | b'\x00\x01'                                               |                                                           |
|(0002, 0003) Media Storage SOP Instance UID               | 1.3.12.2.1107.5.2.19.46234.202105071859329777203889       |                                                           |
|(0002, 0010) Transfer Syntax UID                          | 1.2.840.10008.1.2.1                                       | 1.2.840.10008.1.2.4.70                                    |
|(0002, 0012) Implementation Class UID                     | 1.3.12.2.1107.5.2                                         |                                                           |
|(0008, 0000) Group Length                                 |                                                           | 990                                                       |
|(0010, 0000) Group Length                                 |                                                           | 128                                                       |
|(0008, 0005) Specific Character Set                       | ISO_IR 100                                                | ISO 2022 IR 149                                           |
|(0018, 0000) Group Length                                 |                                                           | 438                                                       |
|(0019, 0000) Private Creator                              |                                                           | 216                                                       |
|(0029, 1008) [CSA Image Header Type]                      | IMAGE NUM 4                                               |                                                           |
|(0010, 0020) Patient ID                                   | 21.05.07-18:12:12-STD-1.3.12.2.1107.5.2.19.46234          | 21.05.07-18_12_12-STD-1.3.12.2.1107.5.2.19.46234          |
|(0051, 0000) Private Creator                              |                                                           | 268                                                       |
|(0029, 0011) Private Creator                              | SIEMENS MEDCOM HEADER2                                    |                                                           |
|(0020, 0000) Group Length                                 |                                                           | 428                                                       |
|(0028, 0000) Group Length                                 |                                                           | 174                                                       |
|(0040, 0000) Group Length                                 |                                                           | 88                                                        |
|(0029, 1009) [CSA Image Header Version]                   | 20210507                                                  |                                                           |
|(0029, 1010) [CSA Image Header Info]                      | b'SV10\x04\x03\x02\x01e\x00\x00\x00M\x00\x00\x00EchoLineP |                                                           |
|(0029, 1018) [CSA Series Header Type]                     | MR                                                        |                                                           |
|(0029, 1019) [CSA Series Header Version]                  | 20210507                                                  |                                                           |
|(0029, 1020) [CSA Series Header Info]                     | b'SV10\x04\x03\x02\x01N\x00\x00\x00M\x00\x00\x00UsedPatie |                                                           |
|(0008, 1140) Referenced Image Sequence                    | [(0008, 1150) Referenced SOP Class UID            UI: MR  | [(0008, 0000) Group Length                        UL: 94
 |
|(0029, 1160) [Series Workflow Status]                     | com                                                       |                                                           |
|(7fe0, 0010) Pixel Data                                   | b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0 | b'\xfe\xff\x00\xe0\x00\x00\x00\x00\xfe\xff\x00\xe0R\xc3\x |
|(0029, 0010) Private Creator                              | SIEMENS CSA HEADER                                        |                                                           |
|(7fe0, 0000) Group Length                                 |                                                           | 115574                                                    |
|(fffc, fffc) Data Set Trailing Padding                    | b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0\x00\x00\x0 |                                                           |

#### 2.2.2. MR -> PACON

(1) PACS에 의해서 추가되었던 private tag 삭제, 기존 MR에 있던 private tag 복원, (2) PatientID, PatientName, StudyID 삭제. (3) meta 정보 추가(이건 pacon에 의해서)가 이루어진다.

|Tags | MR | PACON |
|---|---|---|
|(0002, 0000) File Meta Information Group Length           | 64                                                        | 204                                                       |
|(0002, 0001) File Meta Information Version                |                                                           | b'\x00\x01'                                               |
|(0002, 0003) Media Storage SOP Instance UID               |                                                           | 1.3.12.2.1107.5.2.19.46234.202105071859329777203889       |
|(0002, 0012) Implementation Class UID                     |                                                           | 1.3.6.1.4.1.30071.8                                       |
|(0002, 0013) Implementation Version Name                  |                                                           | fo-dicom 4.0.7                                            |
|(0002, 0016) Source Application Entity Title              |                                                           | VIEWREX                                                   |
|(0008, 0000) Group Length                                 | 990                                                       |                                                           |
|(0010, 0000) Group Length                                 | 128                                                       |                                                           |
|(0018, 0000) Group Length                                 | 438                                                       |                                                           |
|(0019, 0000) Private Creator                              | 216                                                       |                                                           |
|(0008, 0005) Specific Character Set                       | ISO 2022 IR 149                                           |                                                           |
|(0020, 0000) Group Length                                 | 428                                                       |                                                           |
|(0028, 0000) Group Length                                 | 174                                                       |                                                           |
|(0010, 0010) Patient's Name                               | Yoon                                                      |                                                           |
|(0010, 0020) Patient ID                                   | 21.05.07-18_12_12-STD-1.3.12.2.1107.5.2.19.46234          |                                                           |
|(0008, 0030) Study Time                                   | 181602.406000                                             | 181602.000000                                             |
|(0051, 0000) Private Creator                              | 268                                                       |                                                           |
|(0020, 0010) Study ID                                     | 1                                                         |                                                           |
|(0029, 0011) Private Creator                              |                                                           | SIEMENS MEDCOM HEADER2                                    |
|(0008, 0090) Referring Physician's Name                   | 3081                                                      |                                                           |
|(0040, 0000) Group Length                                 | 88                                                        |                                                           |
|(0029, 1008) [CSA Image Header Type]                      |                                                           | IMAGE NUM 4                                               |
|(0029, 1009) [CSA Image Header Version]                   |                                                           | 20210507                                                  |
|(0029, 1010) [CSA Image Header Info]                      |                                                           | b'SV10\x04\x03\x02\x01e\x00\x00\x00M\x00\x00\x00EchoLineP |
|(0029, 1018) [CSA Series Header Type]                     |                                                           | MR                                                        |
|(0029, 1019) [CSA Series Header Version]                  |                                                           | 20210507                                                  |
|(0029, 1020) [CSA Series Header Info]                     |                                                           | b'SV10\x04\x03\x02\x01N\x00\x00\x00M\x00\x00\x00UsedPatie |
|(0008, 1140) Referenced Image Sequence                    | [(0008, 0000) Group Length                        UL: 94
 | [(0008, 1150) Referenced SOP Class UID            UI: MR  |
|(0029, 1160) [Series Workflow Status]                     |                                                           | com                                                       |
|(0029, 0010) Private Creator                              |                                                           | SIEMENS CSA HEADER                                        |
|(7fe0, 0000) Group Length                                 | 115574                                                    |                                                           |

#### 2.2.3. PACON -> MR

(1) 기존 private tag들의 삭제, (2) PatientID, PatientName을 임의로 추가, (3)PatientSex는 other로 임의로 변경이 이루어진다.

|Tags | PACON | MR |
|---|---|---|
|(0002, 0000) File Meta Information Group Length           | 204                                                       | 64                                                        |
|(0002, 0001) File Meta Information Version                | b'\x00\x01'                                               |                                                           |
|(0002, 0003) Media Storage SOP Instance UID               | 1.3.12.2.1107.5.2.19.46234.202105071859329777203889       |                                                           |
|(0002, 0012) Implementation Class UID                     | 1.3.6.1.4.1.30071.8                                       |                                                           |
|(0002, 0013) Implementation Version Name                  | fo-dicom 4.0.7                                            |                                                           |
|(0002, 0016) Source Application Entity Title              | VIEWREX                                                   |                                                           |
|(0008, 0000) Group Length                                 |                                                           | 956                                                       |
|(0010, 0000) Group Length                                 |                                                           | 92                                                        |
|(0008, 0005) Specific Character Set                       |                                                           | ISO 2022 IR 149                                           |
|(0018, 0000) Group Length                                 |                                                           | 438                                                       |
|(0019, 0000) Private Creator                              |                                                           | 216                                                       |
|(0029, 1008) [CSA Image Header Type]                      | IMAGE NUM 4                                               |                                                           |
|(0010, 0010) Patient's Name                               |                                                           | UnKnown                                                   |
|(0010, 0020) Patient ID                                   |                                                           | UnKnown                                                   |
|(0010, 0030) Patient's Birth Date                         | 19940801                                                  | 20101104                                                  |
|(0010, 0040) Patient's Sex                                | M                                                         | O                                                         |
|(0051, 0000) Private Creator                              |                                                           | 268                                                       |
|(0029, 0011) Private Creator                              | SIEMENS MEDCOM HEADER2                                    |                                                           |
|(0020, 0000) Group Length                                 |                                                           | 398                                                       |
|(0028, 0000) Group Length                                 |                                                           | 174                                                       |
|(0040, 0000) Group Length                                 |                                                           | 88                                                        |
|(0029, 1009) [CSA Image Header Version]                   | 20210507                                                  |                                                           |
|(0029, 1010) [CSA Image Header Info]                      | b'SV10\x04\x03\x02\x01e\x00\x00\x00M\x00\x00\x00EchoLineP |                                                           |
|(0029, 1018) [CSA Series Header Type]                     | MR                                                        |                                                           |
|(0029, 1019) [CSA Series Header Version]                  | 20210507                                                  |                                                           |
|(0029, 1020) [CSA Series Header Info]                     | b'SV10\x04\x03\x02\x01N\x00\x00\x00M\x00\x00\x00UsedPatie |                                                           |
|(0008, 1140) Referenced Image Sequence                    | [(0008, 1150) Referenced SOP Class UID            UI: MR  | [(0008, 0000) Group Length                        UL: 94
 |
|(0029, 1160) [Series Workflow Status]                     | com                                                       |                                                           |
|(0029, 0010) Private Creator                              | SIEMENS CSA HEADER                                        |                                                           |
|(7fe0, 0000) Group Length                                 |                                                           | 115574                                                    |

### 2.3. PACS로부터 받은 다이콤을 worker에 recon 요청 시 에러 발생

### 2.4. PACON을 거친 다이콤이 원본 study와 동일한 study로 인식되지 않음

내일 물어볼 내용

## 3. 그 외

### 3.1. `c-find`를 통해 query 가능한 key

무슨 이유인지는 모르겠는데, `pydicom`으로 query를 하면 내가 명시하지 않은 key들도 전부 가져온다. Query 결과는 아래와 같다.
```
(0008, 0000) Group Length                        UL: 104
(0008, 0005) Specific Character Set              CS: 'ISO 2022 IR 149'
(0008, 0020) Study Date                          DA: '20210510'
(0008, 0050) Accession Number                    SH: '3147584'
(0008, 0052) Query/Retrieve Level                CS: 'STUDY'
(0008, 0061) Modalities in Study                 CS: 'DX'
(0008, 0090) Referring Physician's Name          PN: ''
(0008, 1030) Study Description                   LO: 'Chest PA'
(0010, 0000) Group Length                        UL: 56
(0010, 0010) Patient's Name                      PN: '권오현'
(0010, 0020) Patient ID                          LO: '17027839'
(0010, 0030) Patient's Birth Date                DA: '19841225'
(0010, 0040) Patient's Sex                       CS: 'M'
(0020, 0000) Group Length                        UL: 98
(0020, 000d) Study Instance UID                  UI: 1.2.643.18619941.2557901.92021510.914330
(0020, 1200) Number of Patient Related Studies   IS: "3"
(0020, 1202) Number of Patient Related Series    IS: "1"
(0020, 1204) Number of Patient Related Instances IS: "1"
(0020, 1206) Number of Study Related Series      IS: "1"
(0020, 1208) Number of Study Related Instances   IS: "1"
```
