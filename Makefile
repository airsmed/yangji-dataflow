DIR="$(pwd)"
DCM_DIR="/mnt/airsfs2/ChangyoungKwon/datasets/yangji-report"

.PHONY: download-files
download-files:
	@echo "---download files from fileserver---"
	@/bin/bash ./download_files.sh
	@echo "done"

.PHONY: download-deps
download-deps:
	@echo "---download all dependencies---"
	@pip3 install -r requirements.txt > /dev/null
	@echo "done"

.PHONY: compare
compare: | download-files download-deps
	@echo "compare each dicoms"
	python3 ./dcmcompare.py ./01-device ./02-cmove-before
	# python3 ./dcmcompare.py ./02-cmove-before ./03-cmove-after
	# python3 ./dcmcompare.py ./03-cmove-after ./04-cstore-before
	# python3 ./dcmcompare.py ./04-cstore-before ./05-cstore-after
	@echo "done"
