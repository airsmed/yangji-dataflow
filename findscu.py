#!/usr/bin/python3
from datetime import date, timedelta
import pydicom
import logging
import argparse
from pynetdicom import AE
from pynetdicom.sop_class import StudyRootQueryRetrieveInformationModelFind, PatientRootQueryRetrieveInformationModelFind

# ae settings
AET         = "SWIFTMR_NODE"
AET_PORT    = 30001
AEC         = "VIEXREX"
AEC_HOST    = "192.168.9.3"
AEC_PORT    = 310
MAX_DAYS    = 10
INFO_MODEL  = StudyRootQueryRetrieveInformationModelFind    # convertable via cli
LEVEL       = "STUDY"                                       # convertable via cli

# arguments 
parser = argparse.ArgumentParser("findscu", description="c-find bringing studies within specified days")
parser.add_argument("-p", "--patient", default=False, help="acts in patient mode", action="store_true")
parser.add_argument("-o", "--options", default=False, help="print options", action="store_true")
parser.add_argument("-d", "--debug", default=False, help="print additonal debug-info", action="store_true")
parser.add_argument("-n", "--within", default=1, help="past n days(default 1)")

args = parser.parse_args()
if args.options:
    print("{0:10}: {1:<20}".format("AET", AET))
    print("{0:10}: {1:<20}".format("AEC", AEC))
    print("{0:10}: {1:<20}".format("AEC_HOST", AEC_HOST))
    print("{0:10}: {1:<20}".format("AEC_PORT", AEC_PORT))
    exit(0)

if args.debug:
    logging.basicConfig(level=logging.DEBUG)

if args.patient:
    INFO_MODEL = PatientRootQueryRetrieveInformationModelFind
    LEVEL = "Patient"

args.within = 1 if args.within <= 0 else args.within
args.within = args.within if args.within <= MAX_DAYS else MAX_DAYS

# create AE
def newAE() -> AE:
    ae = AE(ae_title=AET.encode('utf-8'))
    ae.add_requested_context(INFO_MODEL)
    return ae

def query_dataset(delta:int) -> pydicom.Dataset:
    from_day = date.today() - timedelta(days=delta)
    ds = pydicom.Dataset()
    ds.SpecificCharacterSet = "ISO 2022 IR 149"  # character set
    ds.QueryRetrieveLevel   = LEVEL.upper()
    ds.StudyDate            = from_day.strftime("%Y%m%d") + "-"
    return ds

def query(ae: AE, dataset: pydicom.Dataset):
    assoc = ae.associate(AEC_HOST, AEC_PORT, ae_title=AEC)
    if assoc.is_established:
        responses = assoc.send_c_find(dataset, INFO_MODEL)
        for status, identifier in responses:
            if status:
                print('C-FIND query status: 0x{0:04X}'.format(status.Status))
                print(identifier)
            else:
                print('Connection timed out, was aborted or received invalid response')
        assoc.release()
    else:
        print('Association rejected, aborted or never connected')

if __name__ == "__main__":
    ae = newAE()
    query(ae, query_dataset(args.within))
