from rauth import OAuth2Service

TOKEN_ENDPOINT = "https://auth.airsmed.io/auth/realms/abc-gwanak/protocol/openid-connect/token"
TOKEN_CLIENT_ID = "pacon"
TOKEN_CLIENT_SECRET = "2e8a7526-5425-4ba2-a61e-e67d0b4f68c1"

API_ENDPOINT = ""

def get_token() -> int:
    auth_service = OAuth2Service(TOKEN_CLIENT_ID, TOKEN_CLIENT_SECRET, "KeyCloak", TOKEN_ENDPOINT)
    return auth_service.get_raw_access_token().status_code

def recon_request(filename:str) -> str:
    return ""

if __name__ == "__main__":
    print(get_token())
