#!/sbin/python
import pydicom as dcm
import shutil
import itertools
from pydicom import errors
import logging
from typing import  Dict, Set, Tuple, Generator
from pathlib import Path
import argparse
import sys

# all constants used in program
terminal_width = shutil.get_terminal_size()
TAG_WIDTH = int(terminal_width.columns * 0.30)
MAX_VALUE_LEN = int(terminal_width.columns * 0.30)
DCM_EXT = ["dcm", "IMA"]

# change all dicom in 
parser = argparse.ArgumentParser("dcmcompare", description="change dicom in several ways")
parser.add_argument("series_a_dir", help="directory containg the first series")
parser.add_argument("series_b_dir", help="directory containg the second series")

# argument validations
args = parser.parse_args()
if args.series_a_dir == ""  or not Path(args.series_a_dir).exists():
    print(f"invalid path \"{args.series_a_dir}\"")
    exit(1)

if args.series_b_dir == ""  or not Path(args.series_b_dir).exists():
    print(f"invalid path \"{args.series_a_dir}\"")
    exit(1)

abs_series_a_dir = str(Path(args.series_a_dir).absolute())
abs_series_b_dir = str(Path(args.series_b_dir).absolute())

logging.debug(f"source_dir : {abs_series_a_dir}")
logging.debug(f"out_dir: {abs_series_b_dir}")

# dcm_files returns the dicom files inside the directory, in min depth
def dcm_filepaths(dirname:str) -> itertools.chain[Path]:
    logging.debug(f"label: dcm_files, event: start, input: {dirname}")
    dcm_patterns = [f"*.{ext}" for ext in DCM_EXT]
    return itertools.chain(*[Path(dirname).glob(pattern) for pattern in dcm_patterns])

# dcm_first returns the dicom file of lowest instance number, throws error if no instance number exists or every file is unparsable
# assumption: all dicom files within directory is valid, and same series(throw exception if not), and has InstanceNumber
def dcm_first(dirname:str) -> str:
    logging.debug(f"start sort on {dirname}")
    lowest_number:int = sys.maxsize
    candidate:str = ""
    series_uid:str = ""

    dicom_exists:bool = next(dcm_filepaths(dirname), None) != None
    if not dicom_exists:
        logging.error(f"given directory does not contain any dicom {dirname}")
        raise Exception(f"given directory does not contain any dicom")
    for filepath in dcm_filepaths(dirname):
        filename = str(filepath)
        try:
            data = dcm.dcmread(filename)
            if data.InstanceNumber is None:
                logging.error(f"no instance number on file {filename}")
                raise Exception("no instance number available")
            if data.SeriesInstanceUID is None:
                logging.error(f"no series instance uid given {filename}")
                raise Exception("unable to specify series")
            if(series_uid != "" and data.SeriesInstanceUID != series_uid):
                logging.error(f"multiple series given {filename}")
            series_uid = str(data.SeriesInstanceUID) if series_uid == "" else series_uid
            instance_number = int(data.InstanceNumber)
            if lowest_number > instance_number:
                lowest_number = instance_number
                candidate = filename
        except errors.InvalidDicomError as err:
            logging.error(f"try to parse invalid dicom file {filename}, {err}")
            raise err
    return candidate

# dcm_diff returns differnce b/w two dicom files
# error if two given name is invalid dicom file
def dcm_diff(dcm_a: str, dcm_b: str) -> str:
    keys:Set = set()
    meta_keys:Set = set()
    diffs:Dict[Tuple[str, str], Tuple[str, str]] = dict()

    # parse dicoms, erorr if dicom is unable to parse
    dataset_a = dcm.dcmread(dcm_a)
    dataset_b = dcm.dcmread(dcm_b)

    # append keys
    keys.update(dataset_a.keys())
    keys.update(dataset_b.keys())

    # append meta_keys
    meta_keys.update(dataset_a.file_meta.keys())
    meta_keys.update(dataset_b.file_meta.keys())

    # compare meta tags
    for key in meta_keys:
        data_a = dataset_a.file_meta.get(key)
        data_b = dataset_b.file_meta.get(key)
        tag_description = data_a.description() if data_a is not None else data_b.description()
        if (data_a is None or data_b is None) or \
                str(data_a.value) != str(data_b.value):
            serialized_a = "" if data_a is None else str(data_a.value)
            serialized_b = "" if data_b is None else str(data_b.value)

            # shortening
            serialized_a = serialized_a[:MAX_VALUE_LEN] if len(serialized_a) > MAX_VALUE_LEN else serialized_a
            serialized_b = serialized_b[:MAX_VALUE_LEN] if len(serialized_b) > MAX_VALUE_LEN else serialized_b
            diffs[(key, tag_description)] = (serialized_a, serialized_b)

    # compare tags
    for key in keys:
        data_a = dataset_a.get(key)
        data_b = dataset_b.get(key)
        tag_description = data_a.description() if data_a is not None else data_b.description()
        if (data_a is None or data_b is None) or \
                str(data_a.value) != str(data_b.value):
            serialized_a = "" if data_a is None else str(data_a.value)
            serialized_b = "" if data_b is None else str(data_b.value)

            # shortening
            serialized_a = serialized_a[:MAX_VALUE_LEN] if len(serialized_a) > MAX_VALUE_LEN else serialized_a
            serialized_b = serialized_b[:MAX_VALUE_LEN] if len(serialized_b) > MAX_VALUE_LEN else serialized_b
            diffs[(key, tag_description)] = (serialized_a, serialized_b)

    return "\n".join([f"|{f'{k} {d}'.ljust(TAG_WIDTH)} | {v1.ljust(MAX_VALUE_LEN)} | {v2.ljust(MAX_VALUE_LEN)} |" for (k, d), (v1, v2) in diffs.items()])

if __name__ == "__main__":
    print(dcm_diff(dcm_first(abs_series_a_dir), dcm_first(abs_series_b_dir)))

